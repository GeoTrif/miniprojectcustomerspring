package com.sda.miniproject.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sda.miniproject.model.Customer;

public class CustomerMapper {
	public List<Customer> mapResultSetToCustomers(ResultSet resultSet) {
		List<Customer> customers = new ArrayList<>();

		try {
			while (resultSet.next()) {
				Customer customer = new Customer();
				customer.setCustomerId(resultSet.getInt("customer_id"));
				customer.setName(resultSet.getString("name"));
				customer.setDateOfBirth(resultSet.getString("dateOfBirth"));
				customer.setAddressId(resultSet.getInt("address_id"));
				customers.add(customer);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return customers;
	}
}
