package com.sda.miniproject.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class ConnectionUtilJDBC {
	private String url;
	private String user;
	private String password;
	private Connection connection;

	public ConnectionUtilJDBC() { // singleton lazy;
	}

	public ConnectionUtilJDBC(String url, String user, String password) {
		this.url = url;
		this.user = user;
		this.password = password;
	}

	public Connection makeConnection() {

		if (connection != null) {
			return connection;
		}

		try {
			connection = DriverManager.getConnection(url, user, password);

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return connection;
	}
}
