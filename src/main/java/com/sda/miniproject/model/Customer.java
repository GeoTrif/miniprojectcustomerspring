package com.sda.miniproject.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.ManyToAny;
import org.springframework.stereotype.Component;

@Entity
@Table(name = "customers")
public class Customer {

	@Id
	@Column(name = "customer_id")
	private int customerId;

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	@Column(name = "name")
	private String name;
	@ManyToOne
	@JoinColumn(name = "address_id")
	private Address address;
	@Column(name = "dateOfBirth")
	private String dateOfBirth;
	@Transient
	private int addressId;

	public Customer() {

	}

	public Customer(String name, Address address, String dateOfBirth) {
		this.name = name;
		this.address = address;
		this.dateOfBirth = dateOfBirth;
	}

	public Customer(String name, String dateOfBirth, int addressId) {
		this.name = name;
		this.dateOfBirth = dateOfBirth;
		this.addressId = addressId;
	}

	public Customer(String name, Address address, String dateOfBirth, int addressId) {
		this.name = name;
		this.address = address;
		this.dateOfBirth = dateOfBirth;
		this.addressId = addressId;
	}

	public Customer(int customerId, String name, Address address, String dateOfBirth, int addressId) {
		this.customerId = customerId;
		this.name = name;
		this.address = address;
		this.dateOfBirth = dateOfBirth;
		this.addressId = addressId;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return this.name;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public Address getAddress() {
		return this.address;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getDateOfBirth() {
		return this.dateOfBirth;
	}

	public int getAddressId() {
		return addressId;
	}

	public void setAddressId(int addressId) {
		this.addressId = addressId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + customerId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Customer other = (Customer) obj;
		if (customerId != other.customerId)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Customer [name=" + name + ", address=" + address + ", dateOfBirth=" + dateOfBirth + ", AddressId="
				+ addressId + "]";
	}

}
