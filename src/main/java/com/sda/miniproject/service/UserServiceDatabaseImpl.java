package com.sda.miniproject.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sda.miniproject.dao.CustomerDao;
import com.sda.miniproject.dao.UserDao;
import com.sda.miniproject.model.User;

@Service
@Transactional
public class UserServiceDatabaseImpl implements UserService {

	@Autowired
	private UserDao userDao;

	public UserDao getUserDao() {
		return userDao;
	}

	public void setUserDao(UserDao u) {
		this.userDao = u;
	}

	@Override
	public User getUserByUsername(String username) {
		System.out.println("UserServiceDatabaseImpl - getUserByUsername");
		return userDao.getUserByUsername(username);
	}

}
