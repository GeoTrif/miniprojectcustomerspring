package com.sda.miniproject.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.sda.miniproject.model.Role;

/**
 * This class will be used for retrieving the user from the database and
 * providing it to the authentification manager.
 * 
 * @author GeoTrif
 *
 */

@Service
@Transactional
public class AuthenticationService implements UserDetailsService {

	@Autowired
	private UserService userService;

	/**
	 * Method converts our user entity object into UserDetails spring objects.
	 */
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		System.out.println("AuthentificationService loading user for username:" + username);
		com.sda.miniproject.model.User domainUser = userService.getUserByUsername(username);

		return new User(domainUser.getUsername(), domainUser.getPassword(), true, true, true, true,
				getAuthorities(domainUser));
	}

	/**
	 * Method converts our database user roles into spring authorities objects.
	 * 
	 * @param user
	 * @return
	 */
	public Collection<? extends GrantedAuthority> getAuthorities(com.sda.miniproject.model.User user) {
		System.out.println();
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		for (Role role : user.getRoles()) {
			authorities.add(new SimpleGrantedAuthority(role.getRoleName()));
		}

		return authorities;
	}

}
