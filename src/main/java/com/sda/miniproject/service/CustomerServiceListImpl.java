package com.sda.miniproject.service;

import java.util.Iterator;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;

import com.sda.miniproject.model.Customer;

@Service
@Transactional
@Qualifier("customerServiceListImpl")
public class CustomerServiceListImpl implements CustomerService {

	private List<Customer> customersList;

	public List<Customer> getCustomersList() {
		return customersList;
	}

	public void setCustomersList(List<Customer> customersList) {
		this.customersList = customersList;
	}

	@Override
	public String toString() {
		return "CustomerServiceListImpl [customersList=" + customersList + "]";
	}

	public void addCustomerUsingBeans(Customer customer) {
		customersList.add(customer);
	}

	public List<Customer> getAllCustomers() {
		return customersList;
	}

	public void deleteCustomer(Customer customer) {
		Iterator<Customer> iterator = customersList.iterator();
		while (iterator.hasNext()) {
			if (iterator.next().equals(customer)) {
				iterator.remove();
			}
		}
	}

	@Override
	public Customer getCustomerByName(String name) {
		for (Customer customer : customersList) {

			if (customer.getName().equals(name)) {
				return customer;
			}
		}
		return null;
	}

	@Override
	public void deleteCustomerByName(String name) {
		Iterator<Customer> iterator = customersList.iterator();
		while (iterator.hasNext()) {
			if (iterator.next().getName().equals(name)) {
				iterator.remove();
			}
		}

	}

	@Override
	public void deleteCustomerById(int id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateCustomer(Customer customer) {
		// TODO Auto-generated method stub
		
	}

}
