package com.sda.miniproject.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sda.miniproject.dao.CustomerDao;
import com.sda.miniproject.dao.CustomerDaoJDBCImlp;
import com.sda.miniproject.model.Customer;

@Service
@Transactional
public class CustomerServiceDatabaseImpl implements CustomerService {

	@Autowired
	@Qualifier("customerDaoHibernateImpl")
	private CustomerDao customerDao;

	public CustomerDao getCustomerDao() {
		return customerDao;
	}

	public void setCustomerDao(CustomerDao c) {
		this.customerDao = c;
	}

	public void addCustomerUsingBeans(Customer customer) {
		customerDao.addCustomerUsingBeans(customer);
	}

	public List<Customer> getAllCustomers() {
		System.out.println("CustomerServiceDatabaseImpl - getAllCustomers");
		return customerDao.getAllCustomers();
	}

	public void deleteCustomer(Customer customer) {
		System.out.println("CustomerServiceDatabaseImpl - getAllCustomers");
		customerDao.deleteCustomer(customer);

	}

	@Override
	public Customer getCustomerByName(String name) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteCustomerByName(String name) {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteCustomerById(int id) {
		System.out.println("CustomerServiceDatabaseImpl - getAllCustomers");
		customerDao.deleteCustomerById(id);

	}

	@Override
	public void updateCustomer(Customer customer) {
		System.out.println("CustomerServiceDatabaseImpl - updateCustomer");
		customerDao.updateCustomer(customer);

	}

}
