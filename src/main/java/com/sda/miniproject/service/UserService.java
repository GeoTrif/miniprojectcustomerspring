package com.sda.miniproject.service;

import com.sda.miniproject.model.User;

public interface UserService {

	public User getUserByUsername(String username);

}
