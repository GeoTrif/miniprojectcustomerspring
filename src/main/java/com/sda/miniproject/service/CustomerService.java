package com.sda.miniproject.service;

import java.util.List;

import com.sda.miniproject.model.Customer;

public interface CustomerService {

	public void addCustomerUsingBeans(Customer customer);

	public List<Customer> getAllCustomers();

	public void deleteCustomer(Customer customer);

	public Customer getCustomerByName(String name);

	public void deleteCustomerByName(String name);

	public void deleteCustomerById(int id);

	public void updateCustomer(Customer customer);

}
