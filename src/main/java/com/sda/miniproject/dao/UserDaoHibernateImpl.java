package com.sda.miniproject.dao;

import javax.transaction.Transactional;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.sda.miniproject.model.User;

@Repository
@Transactional
public class UserDaoHibernateImpl implements UserDao {

	@Autowired
	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public User getUserByUsername(String username) {
		System.out.println("UserDaoHibernateImpl - getUserByUsername");
		Query query = sessionFactory.getCurrentSession().createQuery("from User where username= :param")
				.setParameter("param", username);

		return (User) query.uniqueResult();
	}

}
