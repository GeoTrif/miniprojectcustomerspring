package com.sda.miniproject.dao;

import com.sda.miniproject.model.User;

public interface UserDao {

	public User getUserByUsername(String username);

}
