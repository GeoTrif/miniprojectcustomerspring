package com.sda.miniproject.dao;

import java.util.List;

import com.sda.miniproject.model.Customer;

public interface CustomerDao {

	public void addCustomerUsingBeans(Customer customer);

	public List<Customer> getAllCustomers();

	public void deleteCustomer(Customer customer);

	public Customer getCustomerByName(String name);

	public void deleteCustomerById(int id);

	public void updateCustomer(Customer customer);

}
