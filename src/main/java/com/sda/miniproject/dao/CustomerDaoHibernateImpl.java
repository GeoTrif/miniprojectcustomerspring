package com.sda.miniproject.dao;

import java.util.List;

import javax.persistence.Transient;
import javax.transaction.Transactional;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.sda.miniproject.model.Customer;

@Repository
@Transactional
public class CustomerDaoHibernateImpl implements CustomerDao {

	@Autowired
	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public void addCustomerUsingBeans(Customer customer) {
		System.out.println("CustomerDaoHibernateImpl - addCustomerUsingBeans");
		Session session = sessionFactory.openSession();
		Transaction transaction = null;

		try {
			transaction = session.beginTransaction();
			session.save(customer);
			transaction.commit();
			session.close();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			session.close();
		}
	}

	public List<Customer> getAllCustomers() {
		System.out.println("CustomerDaoHibernateImpl - getAllCustomers");
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();

		List<Customer> customers = session.createQuery("from Customer").list();

		System.out.println(customers);

		transaction.commit();
		session.close();

		return customers;

	}

	public void deleteCustomer(Customer customer) {
		System.out.println("CustomerDaoHibernateImpl - deleteCustomer");
		Session session = sessionFactory.openSession();
		Transaction transaction = null;

		try {
			transaction = session.beginTransaction();
			session.delete(customer);
			transaction.commit();
			session.close();
		} catch (HibernateException e) {
			if (transaction != null) {
				transaction.rollback();
			}
			session.close();
		}

	}

	@Override
	public Customer getCustomerByName(String name) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteCustomerById(int id) {
		System.out.println("CustomerDaoHibernateImpl - deleteCustomerById");
		Session session = sessionFactory.getCurrentSession();

		Customer customer = (Customer) session.load(Customer.class, id);
		session.delete(customer);

	}

	@Override
	public void updateCustomer(Customer customer) {
		System.out.println("CustomerDaoHibernateImpl - updateCustomer");
		Session session = sessionFactory.getCurrentSession();

		session.update(customer);

	}

}
