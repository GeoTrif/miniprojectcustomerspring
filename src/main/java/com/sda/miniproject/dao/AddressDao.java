package com.sda.miniproject.dao;

import java.util.List;

import com.sda.miniproject.model.Address;
import com.sda.miniproject.model.Customer;

public interface AddressDao {

	public void addAddressUsingBeans(Address address);

	public List<Address> getAllAddresses();

	public void deleteAddress(Address address);

}
