package com.sda.miniproject.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Repository;

import com.sda.miniproject.model.Customer;
import com.sda.miniproject.utils.ConnectionUtilJDBC;

@Repository
public class CustomerDaoJDBCImlp implements CustomerDao {

	@Autowired
	private ConnectionUtilJDBC connection;

	public ConnectionUtilJDBC getConnection() {
		return connection;
	}

	public void setConnection(ConnectionUtilJDBC connection) {
		this.connection = connection;
	}

	public void addCustomerUsingBeans(Customer customer) {
		String sql = "insert into customerspring.customers(name,dateOfBirth,address_id) values (?,?,?)";
		try {
			PreparedStatement preparedStatement = getConnection().makeConnection().prepareStatement(sql);
			preparedStatement.setString(1, customer.getName());
			preparedStatement.setString(2, customer.getDateOfBirth());
			preparedStatement.setInt(3, customer.getAddressId());
			preparedStatement.executeUpdate();
			preparedStatement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public List<Customer> getAllCustomers() {
		String sql = "select * from customers";
		List<Customer> customers = null;

		try {
			Statement statement = getConnection().makeConnection().createStatement();
			ResultSet resultSet = statement.executeQuery(sql);

			while (resultSet.next()) {
				int id = resultSet.getInt("customer_id");
				String firstName = resultSet.getString("first_name");
				String lastName = resultSet.getString("last_name");
				String address = resultSet.getString("address");
				String city = resultSet.getString("city");

				System.out.println("Customer id: " + id);
				System.out.println("First name: " + firstName);
				System.out.println("Last name: " + lastName);
				System.out.println("Adress: " + address);
				System.out.println("City: " + city);
			}

			resultSet.close();
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return customers;
	}

	public void deleteCustomer(Customer customer) {
		String sql = "delete from customers where name=?";

		try {
			PreparedStatement preparedStatement = getConnection().makeConnection().prepareStatement(sql);
			preparedStatement.setString(1, customer.getName());
			preparedStatement.executeUpdate();
			preparedStatement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public Customer getCustomerByName(String name) {
		String sql = "select * from customers where name=?";

		try {
			PreparedStatement preparedStatement = getConnection().makeConnection().prepareStatement(sql);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void deleteCustomerById(int id) {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateCustomer(Customer customer) {
		// TODO Auto-generated method stub

	}
}
