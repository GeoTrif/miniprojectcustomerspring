package com.sda.miniproject.main;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.sda.miniproject.utils.ConnectionUtilJDBC;

@Configuration
public class MainConfig {

	@Bean
	public ConnectionUtilJDBC dbConnectionUtil() {
		return new ConnectionUtilJDBC();
	}

}
