package com.sda.miniproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.sda.miniproject.service.UserService;

@Controller
@RequestMapping("/userController")
public class UserController {

	@Autowired
	private UserService userService;

	@RequestMapping(value = "getUserByUsername", method = RequestMethod.GET)
	public String getCustomerByName(ModelMap model, @RequestParam("username") String username) {
		model.addAttribute("UserByUsername", userService.getUserByUsername(username));
		return "myFirstSpringJsp";
	}

}
