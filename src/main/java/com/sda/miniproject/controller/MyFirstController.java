package com.sda.miniproject.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.sda.miniproject.dao.CustomerDao;
import com.sda.miniproject.model.Customer;
import com.sda.miniproject.service.CustomerService;
import com.sda.miniproject.service.CustomerServiceListImpl;

/**
 * 
 * @author GeoTrif
 * 
 *         URL Structure:
 * 
 *         localhost:8080/{ProjectName}/{ClassRequestMapping}/{MethodRequestMapping}/
 *
 */
@Controller
@RequestMapping("/myFirstController")
public class MyFirstController {

	@Autowired
	@Qualifier("customerServiceDatabaseImpl")
	private CustomerService customerService;

	@RequestMapping(value = "/showCustomers", method = RequestMethod.GET)
	public String showCustomers(ModelMap model) {
		model.addAttribute("CustomerList", customerService.getAllCustomers());
		return "myFirstSpringJsp";
	}

	@RequestMapping(value = "myFirstSpringMethod/{id}/{id2}", method = RequestMethod.GET)
	public String myFirstSpringMethod(@PathVariable Integer id, @PathVariable Integer id2) {
		System.out.println("Code entered myFirstSpringMethod " + id + "  " + id2);

		return "myFirstSpringJsp";
	}

	@RequestMapping(value = "/mySecondSpringMethod", method = RequestMethod.GET)
	public String mySecondSpringMethod(@RequestParam("id") Integer id, @RequestParam("id2") Integer id2) {
		System.out.println("Code entered in mySecondSpringMethod: " + id + "   " + id2);

		return "myFirstSpringJsp";
	}

	@RequestMapping(value = "/myThirdSpringMethod", method = RequestMethod.GET)
	public String myThirdSpringMethod(@ModelAttribute Customer customer) { // aici de obicei se face cu post.

		return "myFirstSpringJsp";
	}

	@RequestMapping(value = "/myFourthSpringMethod", method = RequestMethod.GET)
	public String myFourthSpringMethod(ModelMap model) {

		model.addAttribute("valueFromApp", "This is what we sent from app"); // (id, value)
		return "myFirstSpringJsp";
	}

	@RequestMapping(value = "/getAllCustomers", method = RequestMethod.GET)
	public String getAllCustomers(ModelMap model) {
		System.out.println("MyFirstController - getAllCustomers");
		model.addAttribute("CustomerList", customerService.getAllCustomers());
		return "myFirstSpringJsp";
	}

	@RequestMapping(value = "getCustomerByName", method = RequestMethod.GET)
	public String getCustomerByName(ModelMap model, @RequestParam("name") String name) {
		model.addAttribute("CustomerByName", customerService.getCustomerByName(name));
		return "myFirstSpringJsp";
	}

	@RequestMapping(value = "deleteCustomerByName", method = RequestMethod.GET)
	public String deleteCustomer(ModelMap model, @RequestParam("name") String name) {
		customerService.deleteCustomerByName(name);
		model.addAttribute("deleteCustomerByName", customerService.getAllCustomers());
		return "myFirstSpringJsp";
	}

	@RequestMapping(value = "/deleteCustomerById", method = RequestMethod.POST)
	public String deleteCustomerById(ModelMap model, @RequestParam("id") Integer id) {
		customerService.deleteCustomerById(id);
		model.addAttribute("CustomerList", customerService.getAllCustomers());
		return "myFirstSpringJsp";
	}

	@RequestMapping(value = "/addCustomer", method = RequestMethod.POST)
	public String addCustomer(ModelMap model, @RequestParam("name") String name,
			@RequestParam("dateOfBirth") String dateOfBirth, @RequestParam("addressId") Integer addressId) {
		Customer newCustomer = new Customer(name, dateOfBirth, addressId);
		customerService.addCustomerUsingBeans(newCustomer);
		model.addAttribute("CustomerList", customerService.getAllCustomers());
		return "myFirstSpringJsp";
	}

	@RequestMapping(value = "/updateCustomer", method = RequestMethod.POST)
	public String updateCustomer(ModelMap model, @RequestParam("name") String name,
			@RequestParam("dateOfBirth") String dateOfBirth, @RequestParam("addressId") Integer addressId) {
		Customer updatedCustomer = new Customer(name, dateOfBirth, addressId);
		customerService.updateCustomer(updatedCustomer);
		model.addAttribute("CustomerList", customerService.getAllCustomers());
		return "myFirstSpringJsp";
	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logoutPage(HttpServletRequest request, HttpServletResponse response) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null) {
			new SecurityContextLogoutHandler().logout(request, response, auth);
		}
		return "redirect:/login?logout";// You can redirect wherever you want, but generally it's a good practice to
										// show login screen again.
	}

}
