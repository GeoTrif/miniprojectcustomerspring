<%@page import="com.sda.miniproject.service.CustomerServiceDatabaseImpl"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<style><%@include file="/WEB-INF/resources/styles/layout.css"%></style>
</head>
<body>
<div class="wrapper row1">
  <header id="header" class="clear">
    <div id="hgroup">
      <h1><a href="#">Basic 90</a></h1>
      <h2>Free HTML5 Website Template</h2>
    </div>
    <form action="#" method="post">
      <fieldset>
        <legend>Search:</legend>
        <input type="text" value="Search Our Website&hellip;" onFocus="this.value=(this.value=='Search Our Website&hellip;')? '' : this.value ;">
        <input type="submit" class="sf_submit" value="submit">
      </fieldset>
    </form>
    <nav>
      <ul>
        <li><a href="#">Text Link</a></li>
        <li><a href="#">Text Link</a></li>
        <li><a href="#">Text Link</a></li>
        <li><a href="#">Text Link</a></li>
        <li class="last"><a href="#">Text Link</a></li>
      </ul>
    </nav>
  </header>
</div>
<div class="wrapper row2">
<div id="container" class="clear">
This is my first html page with Spring.

	<h3>${valueFromApp}</h3>
	${CustomerByName}
	<table border="1">
		<thead>
			<tr>
				<td>Customer id</td>
				<td>Name</td>
				<td>Address</td>
				<td>Date Of Birth</td>
				<td>Address id</td>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${CustomerList}" var="iterator">
				<tr>
					<td><c:out value="${iterator.customerId}"></c:out></td>
					<td><c:out value="${iterator.name}"></c:out></td>
					<td><c:out value="${iterator.address}"></c:out></td>
					<td><c:out value="${iterator.dateOfBirth}"></c:out></td>
					<td><c:out value="${iterator.addressId}"></c:out></td>
					<td><form method="POST"
							action="${pageContext.request.contextPath}/myFirstController/deleteCustomerById?id=${iterator.getCustomerId()}">
							<input class="sf_submit" type="submit" value="Delete">
						</form></td>
					<form method="POST"
						action="${pageContext.request.contextPath}/myFirstController/updateCustomer">
						<tr>
							<td><input type="text" name="name"
								placeholder="Customer Name"></td>
							<td><input type="text" name="dateOfBirth"
								placeholder="Date Of Birth"></td>
							<td><input type="number" name="addressId"
								placeholder="Address Id"></td>
							<td><input class ="sf_submit" type="submit" name="add" value="Update"></td>
						</tr>
					</form>
				</tr>
			</c:forEach>
		</tbody>
	</table>

	<h1>Add New Customer</h1>
	<table>
		<form method="POST"
			action="${pageContext.request.contextPath}/myFirstController/addCustomer">
			<tr>
				<td><input type="text" name="name" placeholder="Customer Name"></td>
				<td><input type="text" name="dateOfBirth"
					placeholder="Date Of Birth"></td>
				<td><input type="number" name="addressId"
					placeholder="Address Id"></td>
				<td><input class = "sf_submit" type="submit" name="add" value="Add"></td>
			</tr>
		</form>
	</table>

	<a href="<c:url value="/myFirstController/logout" />">Logout</a>
	</div>
	</div>
</body>
</html>